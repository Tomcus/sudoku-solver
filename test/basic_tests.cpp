#include <cstddef>
#include <cstdint>
#include <vector>
#include <cassert>
#include "sudoku/solve.hpp"

void just_solve(const std::array<std::int8_t, 9*9>& data) {
    auto result = sdk::solve<3>(data);
    for (std::size_t y = 0; y < 9; ++y) {
        for (std::size_t x = 0; x < 9; ++x) {
            assert(result.get(x, y) != 0);
        }
    }
}

void solve_and_check(const std::array<std::int8_t, 9*9>& input, const std::array<std::int8_t, 9*9>& expected) {
    auto result = sdk::solve<3>(input);
    for (std::size_t y = 0; y < 9; ++y) {
        for (std::size_t x = 0; x < 9; ++x) {
            assert(result.get(x, y) == expected[x + y * 9]);
        }
    }
}

int main() {
    solve_and_check({
        0, 3, 0, 4, 0, 7, 1, 0, 0,
        0, 0, 0, 2, 6, 3, 0, 0, 5,
        0, 0, 6, 8, 0, 5, 7, 0, 3,
        9, 0, 0, 3, 8, 4, 0, 0, 7,
        1, 0, 4, 9, 0, 2, 3, 0, 8,
        0, 7, 3, 0, 5, 0, 0, 0, 2,
        0, 0, 9, 0, 2, 0, 5, 3, 0,
        3, 5, 2, 0, 0, 0, 8, 0, 1,
        4, 0, 0, 5, 3, 0, 0, 6, 9
    }, {
        5, 3, 8, 4, 9, 7, 1, 2, 6,
        7, 4, 1, 2, 6, 3, 9, 8, 5,
        2, 9, 6, 8, 1, 5, 7, 4, 3,
        9, 2, 5, 3, 8, 4, 6, 1, 7,
        1, 6, 4, 9, 7, 2, 3, 5, 8,
        8, 7, 3, 1, 5, 6, 4, 9, 2,
        6, 8, 9, 7, 2, 1, 5, 3, 4,
        3, 5, 2, 6, 4, 9, 8, 7, 1,
        4, 1, 7, 5, 3, 8, 2, 6, 9
    });
    return 0;
}