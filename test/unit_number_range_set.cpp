#include "sudoku/number_range_set.hpp"
#include <algorithm>
#include <cassert>
#include <vector>
#include <cstddef>

template<std::size_t capacity, std::size_t min_val, std::size_t max_val>
void print_set(const sdk::number_range_set<capacity, min_val, max_val>& set) {
    for (std::size_t i = max_val; i >= min_val; --i) {
        if (set.get(i)) {
            std::cout << "1";
        } else {
            std::cout << "0";
        }
    }
    std::cout << "\n";
}

template<std::size_t min_value, std::size_t max_value>
void test_values(const std::vector<std::size_t>& values) {
    constexpr static std::size_t capacity = max_value - min_value + 1;
    sdk::number_range_set<capacity, min_value, max_value> set{false};
    for (auto value: values) {
        set.set(value, true);
    }
    assert(set.size() == values.size());
    for (std::size_t i = min_value; i <= max_value; ++i) {
        if (std::find(values.begin(), values.end(), i) != values.end()) {
            assert(set.get(i) == true);
        } else {
            assert(set.get(i) == false);
        }
    }
}

int main() {
    test_values<1, 9>({5, 1, 4});
    test_values<1, 3>({1, 2, 3});
    test_values<0, 2>({});
    test_values<5, 55>({6, 7, 8, 9, 10, 11, 12, 55, 5, 42, 20});
    test_values<6, 8>({6, 8});
    test_values<1, 128>({1, 5, 42, 69, 128, 77, 55, 48, 13});
}