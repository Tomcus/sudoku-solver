#include "sudoku/solve.hpp"
#include <cstdlib>
#include <string_view>
#include <fstream>

template<std::size_t size>
sdk::sudoku<size> load_and_solve(const std::string_view file_name) noexcept {
    std::ifstream input_file;
    input_file.open(file_name.data());
    if (input_file.good()) {
        sdk::sudoku<size> s;
        for (std::size_t y = 0; y < size*size; ++y) {
            for (std::size_t x = 0; x < size*size; ++x) {
                std::int8_t val;
                input_file >> val;
                s.set(x, y, val);
            }
        }
        s.solve();
        return s;
    } else {
        perror("Can't open the file.");
        exit(1);
    }
}

int main(int argc, const char* args[]) {
    if (argc == 2) {
        load_and_solve<3>(args[1]).print();
    } else {
        printf("USAGE:\n    %s [path_to_file]\n", args[0]);
    }
}