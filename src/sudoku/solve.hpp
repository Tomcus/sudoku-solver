#pragma once

#include "sudoku.hpp"
#include <array>
#include <cstdint>

namespace sdk {
    template<std::size_t size>
    sudoku<size> solve(const std::array<std::int8_t, size*size*size*size> data) noexcept {
        constexpr static auto length = size*size;
        sudoku<size> res;
        for (std::size_t y = 0; y < length; ++y) {
            for (std::size_t x = 0; x < length; ++x) {
                auto val = data[x + y * length];
                if (val != 0) {
                    res.set(x, y, val);
                }
            }
        }
        res.solve();
        return res;
    }
}