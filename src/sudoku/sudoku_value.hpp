#pragma once

#include "number_range_set.hpp"

namespace sdk {

template <size_t size>
struct column: public number_range_set<size*size> { };
template <size_t size>
struct row: public number_range_set<size*size> { };
template <size_t size>
struct field: public number_range_set<size*size> { };

template <size_t size>
struct sudoku_value {
    constexpr sudoku_value():value(0), c(nullptr), r(nullptr), f(nullptr) { }
    
    constexpr void set(column<size>* new_col, row<size>* new_row, field<size>* new_fie) noexcept {
        c = new_col;
        r = new_row;
        f = new_fie;
    }

    constexpr void set(std::int8_t new_value) noexcept {
        value = new_value;
        c->set(new_value, false);
        r->set(new_value, false);
        f->set(new_value, false);
    }

    [[nodiscard]] constexpr bool is_set() const noexcept {
        return value != 0;
    }

    [[nodiscard]] constexpr std::int8_t get_value() const noexcept {
        return value;
    }

    [[nodiscard]] constexpr bool solve() noexcept {
        number_range_set intersection(*c & *r & *f);
        if (intersection.size() == 1) {
            for (std::size_t i = 1; i <= 9; ++i) {
                if (intersection.get(i)) {
                    set(i);
                    return true;
                }
            }
        }
        return false;
    }

protected:
    std::int8_t value;
    column<size>* c;
    row<size>* r;
    field<size>* f;
};

}