#pragma once

#include <bits/c++config.h>
#include <cstdint>
#include <bitset>
#include <span>
#include <iostream>
#include <iomanip>

namespace sdk {
template <std::size_t capacity, std::int64_t min_val=1, std::int64_t max_val=capacity>
struct number_range_set {
    static_assert(capacity > 0, "number_range_set must have at least one value");
    static_assert(capacity == (max_val - min_val + 1), "number_range_set's capacity should be equal to length of inclusive range of <min_val; max_val>");
    static_assert(min_val < max_val, "number_range_set min value must be less then max value");
    using data_storage = std::bitset<capacity>;

    constexpr explicit number_range_set(bool default_val=true) noexcept {
        if (default_val)
            data.set();
        else
            data.reset();
    }

    constexpr explicit number_range_set(const std::bitset<capacity>& data): data(data) { }

    [[nodiscard]] constexpr bool get(std::size_t index) const noexcept {
        if (index < min_val || index > max_val)
            return false;
        index -= min_val;
        return data.test(index);
    }

    constexpr void set(std::size_t index, bool val) noexcept {
        if (index >= min_val && index <= max_val && get(index) != val) {
            index -= min_val;
            data.set(index, val);
        }
    }

    constexpr number_range_set<capacity, min_val, max_val> operator& (const sdk::number_range_set<capacity, min_val, max_val>& other) const noexcept {
        return number_range_set<capacity, min_val, max_val>{data & other.data};
    }

    constexpr number_range_set<capacity, min_val, max_val> operator| (const sdk::number_range_set<capacity, min_val, max_val>& other) const noexcept {
        return number_range_set<capacity, min_val, max_val>{data | other.data};
    }

    constexpr number_range_set<capacity, min_val, max_val> operator^ (const sdk::number_range_set<capacity, min_val, max_val>& other) const noexcept {
        return number_range_set<capacity, min_val, max_val>{data ^ other.data};
    }

    [[nodiscard]] constexpr std::size_t size() const noexcept {
        return data.count();
    }

    [[nodiscard]] constexpr const data_storage& get_data() const noexcept {
        return data;
    }
protected:
    data_storage data{};
};
}