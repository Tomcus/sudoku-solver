#pragma once

#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cassert>
#include <array>
#include <vector>

#include "sudoku_value.hpp"

namespace sdk {
template<size_t size>
struct sudoku {
    static_assert(size > 1);
    constexpr static size_t length = size*size;
    constexpr sudoku() {
        for (std::size_t y = 0; y < length; ++y) {
            for (std::size_t x = 0; x < length; ++x) {
                values[x + y * length].set(
                    columns.data() + x,
                    rows.data() + y,
                    fields.data() + x/size + (y/size) * size
                );
            }
        }
    }

    constexpr sudoku(const std::span<std::int8_t, length * length> data): sudoku() {
        for (std::size_t y = 0; y < length; ++y) {
            for (std::size_t x = 0; x < length; ++x) {
                if (data[x + y * length] != 0)
                    values[x + y * length].set(data[x + y * length]);
            }
        }
    }

    constexpr void set(size_t x, size_t y, std::int8_t val) {
        values[x + y * length].set(val);
    }

    constexpr std::int8_t get(size_t x, size_t y) {
        return values[x + y * length].get_value();
    }

    constexpr void print() const noexcept {
        for (std::size_t y = 0; y < length; ++y) {
            for (std::size_t x = 0; x < length; ++x) {
                auto& val = values[x + y*length];
                if (val.is_set()) {
                    putchar('0' + val.get_value());
                } else {
                    putchar(' ');
                }
                putchar(' ');
            }
            putchar('\n');
        }
        puts("=================================");
    }

    constexpr bool solve() noexcept {
        std::vector<sudoku_value<size>*> to_solve;
        to_solve.reserve(45);
        for (std::size_t y = 0; y < length; ++y) {
            for (std::size_t x = 0; x < length; ++x) {
                auto* val = &values[x + y * length];
                if (!val->is_set())
                    to_solve.emplace_back(val);
            }
        }
        while(true) {
            bool did_something = false;   
            std::vector<sudoku_value<size>*> new_to_solve;
            print();
            for (auto it = to_solve.begin(); it != to_solve.end(); ++it) {
                if ((*it)->solve()) {
                    did_something = true;
                } else {
                    new_to_solve.emplace_back(*it);
                }
            }
            to_solve = new_to_solve;
            if (!did_something) {
                break;
            }
        }
        return to_solve.size() == 0;
    }

protected:
    std::array<column<size>, length> columns;
    std::array<row<size>, length> rows;
    std::array<field<size>, length> fields;
    std::array<sudoku_value<size>, length*length> values;
};
}